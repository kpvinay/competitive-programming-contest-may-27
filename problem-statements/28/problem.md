You are given M distinct points in the plane specified by polar co-ordinates (r, a). No points lie on the origin. Find out the maximum number of points among these points that would lie in a straight line. Note that a is expressed in degrees and ranges from 0 to 360, and is an integer. r is an integer as well, r is non-zero.

**Input Format**

The first line of input is an integer T which is the number of test cases. Then the first line of each test case consists of an integer M. Here M is the number of points in the plane. Then M lines follow consisting of two integers r and a.

**Constraints**

0 < r <= 10^9

0 < a <= 360

**Sample Input**
```
1
4
5 30
6 60
7 30
12 30
```

**Sample Output**
```
3
```