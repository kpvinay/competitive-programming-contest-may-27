### Problem Statement 15

Cricket is one of the most popular games in the world. I am not going to explain all the rules here. Suffice it to say that it is a team game, and that the objective is to score more runs than the opposing team. For more information about the basic rules of cricket, check out the wikipedia article on the game.


If you are familiar with the terms over, no ball, free hit, out, runs, batsmen, bowler, deliveries and batting pair, you have enough knowledge to solve the following problem.


Book cricket is a variant of cricket played in high schools throughout the subcontinent. It’s generally a game for two players, each representing a team of 11 players.


The bowling team flips the pages of a book at random. The batting team writes/notes down the last digit of the right-side (even number pages). If it is 2, 4 or 6, the batsman facing the delivery scores 2, 4 or 6 runs. If it is 0, the batsman is out! If it is 8, the delivery is a no ball and an extra run is added to the total with a free hit coming up.


As usual, an over consists of 6 balls unless a no ball is bowled.


In a book cricket game with 10 overs a side, the side batting first scores R runs with W wickets fallen. Here 0 <= W < 10 (the side never gets all out!). Also assume that there are no “no-balls” bowled.


Find out the number of possible ways the first batting team could have achieved this score. If the score could NOT be achieved, output 0.


**Input Format**


The first line of input consists of the number of test cases T. Then T lines follow with the input format as follows:


R W


Here R is the number of runs scores by the batting team, and W is the number of wickets fallen.


**Output Format**


For each test case, print out the number of ways this score could have been achieved.


**Sample Input**
```
2
201 6
108 6
```

**Sample Output**
```
0
50063860
```

**Note: Get proper constraints for this. Numbers get very large, fast! Perhaps need 5 overs a side then.**